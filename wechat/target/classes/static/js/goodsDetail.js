$(document).ready(function () {

    localStorage.removeItem("price");
    localStorage.removeItem("num");
    localStorage.removeItem("name");
    localStorage.removeItem("pictureUrl");
    localStorage.removeItem("id");

    let id = getUrlParam("id");

    $.ajax({
        url:"goodsController/getById/"+id,
        type:"GET",
        success:function (data) {
            console.log(data);
            $("#goodsName").text(data.name);
            for(let i = 0; i < data.pictureList.length; i++){
                $("#pictureDiv").append(
                    '<div class="swiper-slide"><img src="'+data.pictureList[i].path+'"/></div>'
                );
            }
            $("#introduce").text(data.detail);

            $("#decrease").click(function () {
                if(parseInt($("#num").val()) == 0){
                    alert("商品数不能小于0");
                }
                else{
                    $("#num").val(parseInt($("#num").val())-1);
                }
            });
            $("#increase").click(function () {
                console.log(parseInt($("#num").val())+1);
                $("#num").val(parseInt($("#num").val())+1);
            });

            $("#goodsMaterial").text(data.material);
            $("#goodsWeight").text(data.weight);
            $("#goodsSize").text(data.size);

            $("#order").click(function () {
                localStorage.setItem("price",data.price);
                localStorage.setItem("num",$("#num").val());
                localStorage.setItem("name",data.name);
                localStorage.setItem("pictureUrl",data.pictureList[0].path);
                localStorage.setItem("id",id);
                window.location.href = "order.html?flag=0";
            });
        }
    });
});

//获取url中的参数
function getUrlParam(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg);  //匹配目标参数
    if (r != null) return unescape(r[2]); return null; //返回参数值
}