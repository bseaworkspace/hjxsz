$(document).ready(function () {
    var id = getUrlParam("id");
    $.ajax({
        url:"template/templateDetailShow/"+id,
        type:"GET",
        success:function (data) {
            console.log(data);
            $("#templateName").text(data.name);
            $("#picture1").attr('src', data.picture1);
            $("#picture2").attr('src', data.picture2);
            for(let i = 0; i < data.attributeList.length; i++){
                $("#fb").append(
                    "<option name='"+i+"' value="+data.attributeList[i].price+">"+data.attributeList[i].name+"</option>"
                );
            }

            $("#decrease").click(function () {
                if(parseInt($("#num").val()) == 0){
                    alert("输入错误");
                }
                else{
                    $("#num").val(parseInt($("#num").val())-1);
                }
            });
            $("#increase").click(function () {
                console.log(parseInt($("#num").val())+1);
                $("#num").val(parseInt($("#num").val())+1);
            });


            $("#introduce").html(data.introduce);
            $("#goodsMaterial").text(data.material);
            $("#goodsSize").text(data.size);
            $("#goodsWeight").text(data.weight);

            $("#pictureUpload").click(function () {
                localStorage.setItem("price", $("#fb").val());
                localStorage.setItem("fb", $("#fb").find("option:selected").attr("name"));
                localStorage.setItem("id", id);
                localStorage.setItem("name", data.name);
                localStorage.setItem("num", $("#num").val());
                localStorage.setItem("templateF", data.picture1);
                localStorage.setItem("templateB", data.picture2);
                window.location.href = "custom.html";
            });
        }
    });
});

//获取url中的参数
function getUrlParam(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg);  //匹配目标参数
    if (r != null) return unescape(r[2]); return null; //返回参数值
}