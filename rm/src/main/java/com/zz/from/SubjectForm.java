package com.zz.from;



import lombok.Data;

/**
 * @Description: 描述
 * @Author: llf
 * @CreateDate: 2019/11/4 10:35
 */
@Data
public class SubjectForm {
    private String id;
    private String title;
    private String content;
    private String categoryId;
    private String createTime;
    private String analysis;
    private int level;
    private int type;
    private int trueNum;
    private int completeNum;
    private int viewNum;
}
