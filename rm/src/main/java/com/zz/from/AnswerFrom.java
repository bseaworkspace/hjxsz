package com.zz.from;

import lombok.Data;

/**
 * @Description: 描述
 * @Author: llf
 * @CreateDate: 2019/11/5 16:02
 */
@Data
public class AnswerFrom {
    private String answerList;
    private String quizId;
    private String uid;

}
