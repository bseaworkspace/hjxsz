package com.zz.fh.service;/**
 * @Description: 描述
 * @Author: Bsea
 * @CreateDate: 2019/11/30
 */

import com.zz.fh.entity.Fh;
import com.zz.fh.repository.FhRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * @Description: java类作用描述
 * @Author: Bsea
 * @CreateDate: 2019/11/30$ 16:19$
 */
@Service
public class FhService {
    @Resource
    FhRepository fhRepository;

    public Page<Fh> showall(int page, int size){
        /**
         * Sort.Direction.DESC 表示 从大到小
         * Sort.Direction.ASC  表示 从小到大
         */
        Sort sort=new Sort(Sort.Direction.DESC,"id");
        Pageable pageable= PageRequest.of(page,size,sort);

        return  fhRepository.findAll(pageable);
    }
}

