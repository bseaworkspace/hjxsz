package com.zz.fh.controller;

import com.zz.fh.entity.Fh;
import com.zz.fh.form.FhForm;
import com.zz.util.KeyUtil;
import com.zz.vo.ResponseData;
import com.zz.vo.ResponseDataUtil;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 分红控制类
 * @Author: Bsea
 * @CreateDate: 2019/11/30$ 16:08$
 */

@RestController
@RequestMapping("fh")
public class FhController {

    @RequestMapping("list")
    public ResponseData t3(FhForm fhForm){
        System.out.println(fhForm);
        List<Fh> ls=new ArrayList<>();

        Fh fh=new Fh();
        fh.setAmt(345);
        fh.setId(KeyUtil.genUniqueKey());
        ls.add(fh);
        return ResponseDataUtil.success("sucess",ls);
    }


}
