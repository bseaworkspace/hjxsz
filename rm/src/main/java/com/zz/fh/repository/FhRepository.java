package com.zz.fh.repository;/**
 * @Description: 描述
 * @Author: Bsea
 * @CreateDate: 2019/11/30
 */

import com.zz.fh.entity.Fh;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Description: java类作用描述
 * @Author: Bsea
 * @CreateDate: 2019/11/30$ 16:18$
 */
public interface FhRepository extends JpaRepository<Fh,String> {

}
