package com.zz.fh.form;/**
 * @Description: 描述
 * @Author: Bsea
 * @CreateDate: 2019/11/30
 */

import lombok.Data;

/**
 * @Description: java类作用描述
 * @Author: Bsea
 * @CreateDate: 2019/11/30$ 16:15$
 */
@Data
public class FhForm {
    private int pagesize;
    private int pageno;
}
