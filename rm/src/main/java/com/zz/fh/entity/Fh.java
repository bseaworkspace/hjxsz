package com.zz.fh.entity;/**
 * @Description: 描述
 * @Author: Bsea
 * @CreateDate: 2019/11/30
 */

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Date;

/**
 * @Description: java类作用描述
 * @Author: Bsea
 * @CreateDate: 2019/11/30$ 16:11$
 */
@Entity(name="xsz_fh_main")
@Data
public class Fh {
    @Id
    @Column(length = 30)
    private String id;
    private String toUser;
    private String fromUser;
    private double amt;
    private Date createTime;
    private String comments;

}
