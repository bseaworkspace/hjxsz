package com.zz.entity;/**
 * @Description: 描述
 * @Author: Bsea
 * @CreateDate: 2019/11/15
 */

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @Description: java类作用描述
 * @Author: Bsea
 * @CreateDate: 2019/11/15$ 22:02$
 */
@Entity
public class PieTitle {
    @Id
    @Column(length = 30)
    private String id;
    private String name;
}
