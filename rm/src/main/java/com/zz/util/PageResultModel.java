package com.zz.util;

import java.util.List;

/**
 * @Description: 描述
 * @Author: llf
 * @CreateDate: 2019/11/4 14:24
 */
public class PageResultModel<E> {

    private int number;

    private int size;

    private List<E> content;

    private boolean last;

    private int totalPages;

    private int totalElements;

    private Object sort;

    private boolean first;

    private int numberOfElements;

    private boolean next;

    private boolean previous;

    public int getNumber() {return number;}

    public void setNumber(int number) {this.number = number;}

    public int getTotalPages() {return totalPages;}

    public void setTotalPages(int totalPages) {this.totalPages = totalPages;}

    public int getNumberOfElements() {return numberOfElements;}

    public void setNumberOfElements(int numberOfElements) {this.numberOfElements = numberOfElements;}

    public boolean isFirst() {return first;}

    public void setFirst(boolean first) {this.first = first;}

    public int getTotalElements() {return totalElements;}

    public void setTotalElements(int totalElements) {this.totalElements = totalElements;}

    public Object getSort() {return sort;}

    public void setSort(Object sort) {this.sort = sort;}

    public boolean isLast() {return last;}

    public void setLast(boolean last) {this.last = last;}

    public int getSize() {return size;}

    public void setSize(int size) {this.size = size;}

    public List<E> getContent() {return content;}

    public void setContent(List<E> content) {this.content = content;}

    public boolean hasPrevious() {
        return previous;
    }

    public void setPrevious(boolean previous) {
        this.previous = previous;
    }

    public boolean hasNext() {
        return next;
    }

    public void setNext(boolean next) {
        this.next = next;
    }

}
