
package com.zz.user.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.alibaba.fastjson.JSONObject;
import com.zz.service.RedisTemplateService;
import com.zz.user.entity.Role;
import com.zz.util.JwtUtil;
import com.zz.vo.ResponseDataUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.zz.exception.DataValidationException;
import com.zz.user.dto.BasicUserDto;
import com.zz.user.entity.User;
import com.zz.user.form.BasicUserForm;
import com.zz.vo.ResponseData;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 
 * @Author asuslh
 * @DateTime 2019年10月28日 下午7:58:15 
 */
@Api(value = "用户Controller")
@RestController
@RequestMapping("user")
public class UserController {
	@Resource
	private RestTemplate restTemplate;

	@Resource
	RedisTemplateService redisTemplateService;
	
	@ApiOperation(value = "创建用户基本信息")
	@ApiImplicitParam(name = "basicUserForm", value = "用户实体", required = true, dataType = "BasicUserForm")
	@PostMapping("basicUserForm")
	public ResponseData save(@Valid BasicUserForm basicUserForm,BindingResult bindingResult ){
		if(bindingResult.hasErrors()){
			throw new DataValidationException(bindingResult.getFieldError().getDefaultMessage());
		}


		HttpStatus status = this.restTemplate.postForEntity("http://HOMEWORKS/homework/user/basicUserForm", basicUserForm, BasicUserDto.class).getStatusCode();
		if (status.is2xxSuccessful()) {
			return ResponseDataUtil.success("success");
		} else {
			return ResponseDataUtil.failure("success");
		}
	}

	@ApiOperation(value = "查找用户是否存在")
	@ApiImplicitParam(name = "name", value = "用户名字", required = true, dataType = "String", paramType="path")
	@GetMapping("checkName/{name}")
	public User findByName(@PathVariable("name") String name){
		Map<String, Object> params = new HashMap<>();
		params.put("name", name);
		URI uri = UriComponentsBuilder.fromUriString("http://HOMEWORKS/homework/user/checkName/{name}")
				.build().expand(params).encode().toUri();
		return this.restTemplate.getForEntity(uri, User.class).getBody();
	}

	@ApiOperation(value = "用户登录")
	@ApiImplicitParams({
	@ApiImplicitParam(name = "name", value = "用户名", required = true, dataType = "String", paramType = "path"),
	@ApiImplicitParam(name = "pwd", value = "密码", required = true, dataType = "String", paramType = "path")
	})
	@PostMapping("login")
	public ResponseData findByNameAndPwd(@RequestParam("name") String name, @RequestParam("pwd") String pwd){
//		Map<String, Object> params = new HashMap<>();
//		params.put("name", name);
//		params.put("pwd", pwd);
//		MultiValueMap<String, String> paramMap = new LinkedMultiValueMap<>();
//		paramMap.add("data", JSONObject.toJSONString(params));

		MultiValueMap<String, String> request = new LinkedMultiValueMap<>();
		request.add("name", name);
		request.add("pwd", pwd);

//		URI uri = UriComponentsBuilder.fromUriString("http://HOMEWORKS/homework/user/login/{name}/{pwd}")
//				.build().expand(params).encode().toUri();
		ResponseData responseData=this.restTemplate.postForEntity("http://HOMEWORKS/homework/user/login",request, ResponseData.class).getBody();
		redisTemplateService.set(responseData.msg, (User)responseData.data);
		return  responseData;
	}



	@ApiOperation(value = "登录用户是否是管理员")
	@GetMapping("isAdmin")
	public ResponseData isAdmin(HttpServletRequest request){
		User loginUser= redisTemplateService.get(request.getHeader("TOKEN"),User.class);
		String isAdmin="N";
		for(Role role:loginUser.getRoles()){
			if("管理员".equals(role.getName())){
				isAdmin="Y";
			}
		}
		return  ResponseDataUtil.success(isAdmin);
	}


}
