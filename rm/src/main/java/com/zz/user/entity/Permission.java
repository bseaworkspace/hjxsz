
package com.zz.user.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import org.hibernate.annotations.Proxy;
import lombok.Getter;
import lombok.Setter;

/**
 * @Description: 
 * @Author asuslh
 * @DateTime 2019年10月28日 下午5:08:57 
 */
@Data
public class Permission implements Serializable{

	/**
	  * @Fields serialVersionUID : 
	  */
	
	private static final long serialVersionUID = 7096177578064759974L;
	

	 	/*
	 	 * 编号
	 	 */
	    private String id;
	 	/*
	 	 * url地址
	 	 */
	    private String url;
	    /*
	     * 描述
	     */
	    private String name;


}
