package com.zz.rank.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zz.vo.ResponseData;
import com.zz.vo.ResponseDataUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;


/**
 * @author dell
 * @Description：描述
 * @CreateDate: now
 */

@RestController
@RequestMapping("integral")
public class IntegralController {

    @Autowired
    private RestTemplate restTemplate;

   @GetMapping("total")
    public List<Object[]> orderTotal(){
       JSONArray date=this.restTemplate.getForEntity("http://RANK//integral/total/" , JSONArray.class).getBody();
       List<Object[]> list = JSONObject.parseArray(date.toJSONString(), Object[].class);
      return list;

    }
    @ApiOperation(value = "查看总积分", notes = "根据个人id查看总积分")

    @PostMapping("findtotal")
    public JSONObject  findtoatl(@PathVariable String id){
        HttpHeaders httpHeaders = new HttpHeaders();
        //定义请求参数类型，这里用json所以是MediaType.APPLICATION_JSON
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Map<String, Object>> request = new HttpEntity<Map<String, Object>>(httpHeaders);

        return this.restTemplate.postForEntity("http://RANK//integral/findtotal/" +id, request,JSONObject.class).getBody();


    }
@PutMapping("update")
    public ResponseData updateNum(){

    restTemplate.put("http://RANK//integral/findtotal/",JSONObject.class);
   return ResponseDataUtil.success("更新成功");
}
@PostMapping("add/{useId}/{name}")
    public JSONObject add(@PathVariable("useId") String id,@PathVariable("name") String name) {

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
    HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(headers);
    return   this.restTemplate.postForEntity("http://DOEXERCISE/b/homework/getQuiz/" +id+"/"+name,request, JSONObject.class).getBody();

}



}
