package com.zz.rank.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zz.rank.entity.IntegralForm;
import com.zz.vo.ResponseData;
import com.zz.vo.ResponseDataUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @author dell
 * @Description：描述
 * @CreateDate: now
 */

@RestController
@RequestMapping("integralDetial")
public class IntegralDetialController {

    @Autowired
    private RestTemplate restTemplate;


    @GetMapping("week")
    public List<Object[]> orderTotal(){
        JSONArray date=this.restTemplate.getForEntity("http://RANK//integralDetial/week/" , JSONArray.class).getBody();
        List<Object[]> list = JSONObject.parseArray(date.toJSONString(), Object[].class);
        return list;
    }

    @GetMapping("mouth")
    public List<Object[]> orderMouth(){
        JSONArray date=this.restTemplate.getForEntity("http://RANK//integralDetial/mouth/" , JSONArray.class).getBody();
        List<Object[]> list = JSONObject.parseArray(date.toJSONString(), Object[].class);
        return list;
    }

    @PostMapping("add")
    public ResponseData add(IntegralForm integralForm)
    {
        this.restTemplate.postForEntity("http://DOEXERCISE/b/DoExercise/judge", integralForm, JSONObject.class).getBody();
       return  ResponseDataUtil.success("添加成功");
    }
}
