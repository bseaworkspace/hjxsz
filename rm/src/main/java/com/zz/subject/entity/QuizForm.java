package com.zz.subject.entity;

import lombok.Data;

/**
 * @author mdw
 * @CreateDate2019/11/3
 */
@Data
public class QuizForm {
    private String title;
    private String categoryId;
    private String analysis;
    private String answer;
    private String A;
    private String B;
    private String C;
    private String D;
}
