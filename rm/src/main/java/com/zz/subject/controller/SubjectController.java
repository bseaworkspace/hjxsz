package com.zz.subject.controller;

import com.alibaba.fastjson.JSONObject;


import com.zz.subject.entity.CyForm;
import com.zz.subject.entity.QuizForm;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.AsyncRestOperations;
import org.springframework.web.client.RestTemplate;


import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * @Description: 描述
 * @Author: llf
 * @CreateDate: 2019/11/2 16:56
 */
@RequestMapping("Subject")
@RestController
public class SubjectController {

    @Resource
    private RestTemplate restTemplate;

    @PostMapping("QuizFormAdd")
    public JSONObject add(@Valid  @RequestBody  QuizForm quizForm) {
        System.out.println("前端传过来的Title是：");
        return  this.restTemplate.postForEntity("http://clientdemo1/Subject/QuizFormAdd" , quizForm,  JSONObject.class).getBody();
    }
    @PostMapping("CyFormAdd")
    public JSONObject add(@Valid  @RequestBody CyForm cyForm) {
        System.out.println("前端传过来的Title是：");
        return  this.restTemplate.postForEntity("http://clientdemo1/Subject/CyFormAdd" , cyForm,  JSONObject.class).getBody();
    }
    @GetMapping("findChildren")
    public JSONObject findChildren(){
        return    this.restTemplate.getForEntity("http://clientdemo1/Category/findChildren" ,  JSONObject.class).getBody();
    }
    @GetMapping("page/{page}")
    public JSONObject pageTest(@PathVariable("page") String page){

        System.out.println("startpage=="+page);
        return    this.restTemplate.getForEntity("http://clientdemo1/Subject/page/"+ page,  JSONObject.class).getBody();
    }
    @PostMapping("updateState/{id}")
    public JSONObject updateState(@PathVariable("id") String id)  {

        return this.restTemplate.postForEntity("http://clientdemo1/Subject/updateState/"+ id, null, JSONObject.class).getBody();
    }
    @DeleteMapping("delete/{id}")
    public void delete(@PathVariable("id") String id)  {

       this.restTemplate.delete("http://clientdemo1/Subject/delete/"+ id);
    }
}
