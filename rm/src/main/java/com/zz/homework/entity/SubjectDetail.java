package com.zz.homework.entity;

import lombok.Data;

/**
 * @Description: 描述
 * @Author: llf
 * @CreateDate: 2019/11/3 14:06
 */
//@Entity(name = "tb_detail")
//@Getter
//@Setter

@Data
public class SubjectDetail {
//    @Id
//    @Column(length=50)

    private String id;
    private String detail;
    /**
     * 答案
     */
    private String answer;

    private String back;
//    @JsonIgnore
//    @ManyToOne(cascade= CascadeType.ALL)
//    @JoinColumn(name = "quizId")

    private Subject subject;

}
