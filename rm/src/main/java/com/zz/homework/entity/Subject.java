package com.zz.homework.entity;

import lombok.Data;

import java.util.List;

/**
 * @Description: 题目表
 * @Author: llf
 * @CreateDate: 2019/10/30 16:13
 */

@Data
public class Subject {

    private String id;
    /**
     * 标题
     */
    private String title;
    /**
     * 题目详情
     */
    private String content;
    /**
     * 类型id
     */
    private String categoryId;
    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 解析
     */
    private String analysis;

    /**
     * 题型
     */
    private int type;
    /**
     * 等级
     */
    private int level;
    /**
     * 答对数量
     */
    private int trueNum;
    /**
     * 答题数量
     */
    private int completeNum;
    /**
     * 查看数量
     */
    private int viewNum;

    private List<SubjectDetail> subjectDetails;

    private List<Parameter> parameters;

}
