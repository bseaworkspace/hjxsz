package com.zz.homework.entity;

import lombok.Data;

/**
 * @Description: 类型表
 * @Author: llf
 * @CreateDate: 2019/10/30 16:13
 */

@Data
public class Category {

    private String id;
    /**
     * 标题
     */
    private String title;
    /**
     * 父类id
     */
    private String pid;
    /**
     * 是否存在父类
     */
    private Boolean isPreset;


}
