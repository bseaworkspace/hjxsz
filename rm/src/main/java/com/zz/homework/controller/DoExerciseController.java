package com.zz.homework.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zz.exception.DataValidationException;
import com.zz.from.AnswerFrom;
import com.zz.from.ErrorsFrom;
import com.zz.from.SubjectForm;
import com.zz.homework.entity.Category;
import com.zz.homework.entity.Subject;
import com.zz.homework.entity.SubjectDetail;
import com.zz.service.RedisTemplateService;
import com.zz.user.entity.User;
import com.zz.util.PageResultModel;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.net.URI;
import java.util.*;

/**
 * @Description: 描述
 * @Author: llf
 * @CreateDate: 2019/11/3 20:26
 */
@RestController
@RequestMapping("DoExercise")
public class DoExerciseController {
    @Resource
    private RestTemplate restTemplate;
    @Resource
    RedisTemplateService redisTemplateService;

    /**
     * 父级分类
     * @param page
     * @return
     */
    @GetMapping("showAllSort/{page}")
    public PageResultModel<Category> showAllSort(@PathVariable("page") int page) {

//        Map<String, Object> params = new HashMap<>();
//        params.put("page", page);
//        URI uri = UriComponentsBuilder.fromUriString("http://doExercise/DoExercise/showAllSort/{page}")
//                .build().expand(params).encode().toUri();
//
//        return this.restTemplate.getForEntity(uri, PageResultModel.class).getBody();

        HttpHeaders httpHeaders = new HttpHeaders();
        //定义请求参数类型，这里用json所以是MediaType.APPLICATION_JSON
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Map<String, Object>> request = new HttpEntity<Map<String, Object>>(httpHeaders);
        System.out.println(page+"--page");
        JSONObject jsonObject = this.restTemplate.getForEntity("http://DOEXERCISE/b/DoExercise/showAllSort/"+page, JSONObject.class, request).getBody();
        PageResultModel<Category> categoryList = JSON.toJavaObject(jsonObject, PageResultModel.class);
        return categoryList;
    }

    /**
     * 子级分类
     * @param page
     * @param pid
     * @param title
     * @return
     */
    @GetMapping("showSortByPid/{page}/{pid}/{title}")
    public Map showSortByPid(@PathVariable("page") int page,@PathVariable("pid") String pid, @PathVariable("title") String title) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Map<String, Object>> request2 = new HttpEntity<Map<String, Object>>(httpHeaders);
        return this.restTemplate.getForEntity("http://DOEXERCISE/b/DoExercise/showSortByPid/" + page+"/"+pid+"/"+title , JSONObject.class,request2).getBody();
    }

    /**
     * 根据分类ID找题
     *
     * @param page
     * @return
     */
    @GetMapping("findByCategoryId/{page}/{categoryId}")
    public JSONObject findByCategoryId(@PathVariable("page") String page,@PathVariable("categoryId") String categoryId) {
        HttpHeaders httpHeaders = new HttpHeaders();
        //定义请求参数类型，这里用json所以是MediaType.APPLICATION_JSON
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Map<String, Object>> request1 = new HttpEntity<Map<String, Object>>(httpHeaders);
        return this.restTemplate.getForEntity("http://DOEXERCISE/b/DoExercise/findByCategoryId/" + page + "/" + categoryId, JSONObject.class, request1).getBody();
    }

    /**
     * 选择题正确错误判断
     * @param answerFrom
     * @return
     */
    @PostMapping("judge")
    public JSONObject judge(@RequestBody AnswerFrom answerFrom,HttpServletRequest request) {
//        answerFrom.setUid(request.getHeader("TOKEN"));
//        answerFrom.setUid("1");
        User loginUser= redisTemplateService.get(request.getHeader("TOKEN"),User.class);

        answerFrom.setUid(loginUser.getId());
        return  this.restTemplate.postForEntity("http://DOEXERCISE/b/DoExercise/judge", answerFrom, JSONObject.class).getBody();
    }

    /**
     * 修改subject
     * @param subjectForm
     * @return
     */
    @PutMapping("update")
    public void update(@Valid @RequestBody SubjectForm subjectForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new DataValidationException("修改----验证错误");
        }
        restTemplate.put("http://DOEXERCISE/b/DoExercise/update",subjectForm);
    }

    @PostMapping("saveErrors")
    public JSONObject saveErrors(@RequestBody ErrorsFrom errorsFrom){
        return  this.restTemplate.postForEntity("http://DOEXERCISE/b/DoExercise/saveErrors", errorsFrom, JSONObject.class).getBody();
    }

    /**
     * 找错题
     * @param page
     * @param errorsFrom
     * @return
     */
    @PostMapping ("findErrors")
    public JSONObject findErrors(@PathVariable("page") String page,@RequestBody ErrorsFrom errorsFrom,HttpServletRequest request) {
//        errorsFrom.setUid(request.getHeader("TOKEN"));
        errorsFrom.setUid("1");
        return this.restTemplate.getForEntity("http://DOEXERCISE/b/DoExercise/findErrors/" + page, JSONObject.class).getBody();
    }


    /**
     * 随机出题
     * @param page
     * @return
     */
    @GetMapping("random/{page}")
    public JSONObject random(@PathVariable("page") String page) {
       return this.restTemplate.getForEntity("http://DOEXERCISE/b/DoExercise/random/" + page , JSONObject.class).getBody();
    }
}
