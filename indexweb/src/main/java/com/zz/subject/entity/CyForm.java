package com.zz.subject.entity;

import lombok.Data;

/**
 * @author mdw
 * @CreateDate2019/11/6
 */
@Data
public class CyForm {
    private String title;
    private String content;
    private String categoryId;
    private int level;
    private String back;
    private String detail;
    private String answer;
    private String parameter;

}
