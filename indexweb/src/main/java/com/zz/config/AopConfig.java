package com.zz.config;/**
 * @Description: 描述
 * @Author: Bsea
 * @CreateDate: 2019/11/10
 */

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * @Description: java类作用描述
 * @Author: Bsea
 * @CreateDate: 2019/11/10$ 20:53$
 */
@Configuration
@Aspect
@Slf4j
public class AopConfig {
    /**
     * Pointcut 切入点
     * 匹配cn.controller包下面的所有方法
     */
    @Pointcut("execution(* com.zz..*.*Controller.*(..))")
    public void userCheck(){
        log.info("userCheck************");
    }

    /**
     * 方法执行前
     */
    @Before(value = "userCheck()")
    public void before(JoinPoint joinPoint){
        log.info("2、Before：方法执行开始...");
        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();

        HttpServletRequest request = attributes.getRequest();
        // 记录下请求内容
        log.info("URL : " + request.getRequestURL().toString());
        log.info("TOKEN : " + request.getHeader("TOKEN"));
        log.info("HTTP_METHOD : " + request.getMethod());
        log.info("IP : " + request.getRemoteAddr());
        log.info("CLASS_METHOD : " + joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        log.info("ARGS : " + Arrays.toString(joinPoint.getArgs()));

    }
}
