package com.zz.homework.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zz.from.SourceForm;
import com.zz.vo.ResponseData;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 描述
 * @Author: llf
 * @CreateDate: 2019/11/2 16:56
 */
@RequestMapping("homework")
@RestController
public class HomeworkController {
    @Resource
    private RestTemplate restTemplate;

    @GetMapping("getQuiz/{quizId}")
    public JSONObject getQuiz(@PathVariable String quizId) {
        return this.restTemplate.getForEntity("http://DOEXERCISE/b/homework/getQuiz" + quizId, JSONObject.class).getBody();
    }
    @PostMapping("greetings/{quizId}")
    public JSONObject greetings(@PathVariable String quizId, @RequestBody String source, HttpServletRequest request2) throws Exception {
        SourceForm sourceForm=new SourceForm();
        sourceForm.setSource(source);
//        sourceForm.setUid(request2.getHeader("TOKEN"));
        sourceForm.setUid("1");
        return this.restTemplate.postForEntity("http://DOEXERCISE/b/homework/greetings/" + quizId,sourceForm,JSONObject.class).getBody();
    }

}
