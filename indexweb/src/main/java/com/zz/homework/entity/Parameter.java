package com.zz.homework.entity;

import lombok.Data;

/**
 * @Description: 答案表
 * @Author: llf
 * @CreateDate: 2019/10/30 16:13
 */

@Data
public class Parameter {

    private String id;
    /**
     * 类型
     */
    private String type;
    /**
     * 选项名称
     */
    private String name;
    /**
     * 选项值
     */
    private String value;


    private Subject subject;
}
