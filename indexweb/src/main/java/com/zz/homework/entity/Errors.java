package com.zz.homework.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @Description: 做题集
 * @Author: llf
 * @CreateDate: 2019/11/2 15:51
 */
@Entity(name = "tb_errors")
@Getter
@Setter
public class Errors {
    @Id
    @Column(length = 50)
    private String id;
    /**
     * 用户id
     */
    private String uid;
    /**
     *题目id
     */
    private String quizId;
    /**
     * 对错判断
     */
    private Boolean isWrong;
    /**
     * 第一次做题时间
     */
    private String CreatTime;

}
