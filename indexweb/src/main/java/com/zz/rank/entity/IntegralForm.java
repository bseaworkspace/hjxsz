package com.zz.rank.entity;

import lombok.Data;

/**
 * @author dell
 * @Description：描述
 * @CreateDate: 2019/11/3
 */
@Data
public class IntegralForm {
    private String userId;
    private String type;
    private String createDate;
    private String quizId;

}
