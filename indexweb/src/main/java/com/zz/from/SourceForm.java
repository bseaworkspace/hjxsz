package com.zz.from;/**
 * @Description: 描述
 * @Author: Bsea
 * @CreateDate: 2019/11/11
 */

import lombok.Data;

/**
 * @Description: java类作用描述
 * @Author: Bsea
 * @CreateDate: 2019/11/11$ 9:55$
 */
@Data
public class SourceForm {
    private String source;
    private String uid;

}
