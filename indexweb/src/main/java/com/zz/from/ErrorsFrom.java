package com.zz.from;

import lombok.Data;

/**
 * @Description: 描述
 * @Author: llf
 * @CreateDate: 2019/11/4 10:35
 */
@Data
public class ErrorsFrom {
    private String id;
    /**
     * 用户id
     */
    private String uid;
    /**
     *题目id
     */
    private String quizId;
    /**
     * 对错判断
     */
    private Boolean isWrong;


}
