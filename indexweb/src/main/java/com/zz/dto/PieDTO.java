package com.zz.dto;/**
 * @Description: 描述
 * @Author: Bsea
 * @CreateDate: 2019/11/15
 */

import com.zz.entity.PieRecord;
import lombok.Data;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.Set;

/**
 * @Description: java类作用描述
 * @Author: Bsea
 * @CreateDate: 2019/11/15$ 21:55$
 */
@Data
public class PieDTO {
    private Set<String> titls;
    private Set<PieRecord> pieRecordDTOSet;

}


